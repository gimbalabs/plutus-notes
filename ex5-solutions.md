# 0. Start cardano-node on testnet

## 1. Create a set of private/public signing keys, shelley, and two payment addresses, mary and percy. Fund the addresses with some test Ada.

### Create keys and addresses
```
cardano-cli address key-gen --verification-key-file shelley.vkey --signing-key-file shelley.skey
cardano-cli address key-gen --verification-key-file mary.vkey --signing-key-file mary.skey
cardano-cli address key-gen --verification-key-file percy.vkey --signing-key-file percy.skey
cardano-cli address build --payment-verification-key-file mary.vkey --testnet-magic 1097911063 --out-file mary.addr
cardano-cli address build --payment-verification-key-file percy.vkey --testnet-magic 1097911063 --out-file percy.addr
cat mary.addr
cat percy.addr
```

### Fund wallets
```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in 909665e3d6fa5479139d5e1d271f59d261acc5590b3f8d18442d5cd12a4c9339#0 \
--tx-out addr_test1vps0gztcsalycctg48n4qyr0xrawk4rdudjm5230jxawf0spl4wt2+20000000 \
--tx-out addr_test1vzwa2gz0q95l96vremk3mq2484gukqeh44t067x4hsgyznqqklhvz+20000000 \
--change-address addr_test1qqujcm4zfwddfk3rpe6yg6lrr8fzjpv9fm8qctsu95hk5x0vxxvenc0qul7vppqu28xhw3yjz6r4pmfzaat7akchfyhq4a8m5k \
--out-file tx1.raw

cardano-cli transaction sign \
--tx-body-file tx1.raw \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--out-file tx1.signed

cardano-cli transaction submit \
--testnet-magic 1097911063 \
--tx-file tx1.signed
```

## 2. Define a Mary-era minting script (a "multi-signature" script) that allows shelley to create new Ozymandian tokens. Define a minting policy for the Shelley currency that uses this minting script. Do not use Plutus scripts at this stage.

```
mkdir policy
cd policy
touch ozymandian.script
cd ..
cd wallets
cardano-cli address key-hash --payment-verification-key-file shelley.vkey
```
### Contents of ozymandian.script:
```
{
    "scripts": [
        {
            "keyHash": "ea01924ad1f34f7b3ac8a2804ff88fcbc4a088e18958361b32e22d84",
            "type": "sig"
        }
    ],
    "type": "all"
}
```
### Create Policy ID
```
cd ..
cd policy
cardano-cli transaction policyid --script-file ozymandian.script
```

Note that ID: `2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c`

## 3. Mint 1000 new Ozymandians in the percy address by building and submitting a transaction. Check that they have been successfully minted.

```
cardano-cli transaction build-raw \
--alonzo-era \
--tx-in $TXIN \
--tx-out $PERCY+0+"1000 2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c.ozymandians" \
--mint="1000 2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c.ozymandians" \
--mint-script-file ozymandian.script \
--fee 0 \
--out-file tx3.draft

cardano-cli transaction calculate-min-fee \
--tx-body-file tx3.draft \
--tx-in-count 1 \
--tx-out-count 1 \
--witness-count 2 \
--protocol-params-file protocol.json

cardano-cli transaction build-raw \
--alonzo-era \
--tx-in $TXIN \
--tx-out $PERCY+$CHANGE+"1000 2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c.ozymandians" \
--mint="1000 2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c.ozymandians" \
--mint-script-file ozymandian.script \
--fee $FEE \
--out-file tx3.draft

cardano-cli transaction sign \
--signing-key-file ~/plutus/Alonzo-testnet/jd/exercise5/wallets/percy.skey \
--signing-key-file ~/plutus/Alonzo-testnet/jd/exercise5/wallets/shelley.skey \
--tx-body-file tx3.draft \
--out-file tx3.signed

cardano-cli transaction submit \
--testnet-magic 1097911063 \
--tx-file tx3.signed
```

### Check:
`cardano-cli query utxo –address $(cat percy)`

## 4. Define a second minting script that allows shelley to create new SkyLark tokens. Mint 100 SkyLark tokens and send them to percy. Check that the tokens have been received and then send 75 SkyLark tokens to mary.

### Move some funds so we have the appropriate UTXOs:
```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $TXIN \
--tx-out $PERCY+5000000+"1000 2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c.ozymandians" \
--change-address $PERCY \
--out-file tx5.raw

cardano-cli transaction sign \
--tx-body-file tx5.raw \
--signing-key-file ~/plutus/Alonzo-testnet/jd/exercise5/wallets/percy.skey \
--testnet-magic 1097911063 \
--out-file tx5.signed

cardano-cli transaction submit \
--testnet-magic 1097911063 \
--tx-file tx5.signed
```

### Then it will be easier to mint some SkyLarks:

```
cardano-cli transaction build-raw \
--alonzo-era \
--tx-in $TXIN \
--tx-out $PERCY+0+"75 2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c.SkyLark" \
--mint="75 2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c.SkyLark" \
--mint-script-file ozymandian.script \
--fee 0 \
--out-file tx4.draft

cardano-cli transaction calculate-min-fee \
--tx-body-file tx4.draft \
--tx-in-count 1 \
--tx-out-count 1 \
--witness-count 2 \
--protocol-params-file protocol.json

cardano-cli transaction build-raw \
--alonzo-era \
--tx-in $TXIN \
--tx-out $PERCY+$CHANGE+"75 2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c.SkyLark" \
--mint="75 2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c.SkyLark" \
--mint-script-file ozymandian.script \
--fee $FEE \
--out-file tx4.draft

cardano-cli transaction sign \
--signing-key-file ~/plutus/Alonzo-testnet/jd/exercise5/wallets/percy.skey \
--signing-key-file ~/plutus/Alonzo-testnet/jd/exercise5/wallets/shelley.skey \
--tx-body-file tx4.draft \
--out-file tx4.signed

cardano-cli transaction submit \
--testnet-magic 1097911063 \
--tx-file tx4.signed
```

### Finally, send those to Mary
```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $TXIN \
--tx-out $MARY+10000000+"75 2e1b5368d0ca3dca0b9d0600809ee898880bf1e5de85419251a73c0c.SkyLark" \
--change-address $PERCY \
--out-file tx6.raw

cardano-cli transaction sign \
--tx-body-file tx6.raw \
--signing-key-file ~/plutus/Alonzo-testnet/jd/exercise5/wallets/percy.skey \
--testnet-magic 1097911063 \
--out-file tx6.signed

cardano-cli transaction submit \
--testnet-magic 1097911063 \
--tx-file tx6.signed
```

## Quick questions:
5. What is the least amount of Ada that you need to keep in the mary and percy addresses? What is the least amount of Ozymandians or SkyLarks that you can keep in an address?
6. You want to burn some of your Ozymandians in the percy address_._ How do you do this? What happens to your Ada balances when you burn your tokens?

## 7. Define a Plutus minting script that allows you to mint a variable number of Ozymandian and SkyLark tokens (with the numbers supplied via a redeemer). Verify that this works as you expect.

#### Make a quick collateral UTXO

```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $TXIN \
--tx-out $PERCY+2000000 \
--change-address $PERCY \
--out-file collateral.raw

cardano-cli transaction sign \
--tx-body-file collateral.raw \
--signing-key-file $PERCYKEY \
--testnet-magic 1097911063 \
--out-file collateral.signed

cardano-cli transaction submit \
--testnet-magic 1097911063 \
--tx-file collateral.signed
```

Once `.plutus` script is compiled:

```
cardano-cli transaction policyid --script-file example-minter.plutus
POLICYID=

```

Then use the script to mint
```
cardano-cli transaction build \
--alonzo-era \
$MAGIC \
--tx-in $TXIN \
--tx-out $PERCY+2500000+"300 $POLICYID.Ozymandian" \
--change-address $PERCY \
--mint="300 $POLICYID.Ozymandian" \
--mint-redeemer-value 300 \
--mint-script-file example-minter-ii.plutus \
--tx-in-collateral $COLLATERAL \
--protocol-params-file protocol.json \
--out-file check-amount2.raw

cardano-cli transaction sign \
--signing-key-file $PERCYKEY \
$MAGIC \
--tx-body-file check-amount2.raw \
--out-file check-amount2.signed

cardano-cli transaction submit --tx-file check-amount2.signed $MAGIC
```

#### BUT: What I really want to do now is allow the user to mint any number of tokens, passed as parameter, and conserve the same PolicyID.

- Is it true that the PolicyID will change completely for each script? Is there any way to re-use a Policy ID?

#### In Summary, we need a minimum of four files:
1. cabal.project
2. plutus-example.cabal
3. plutus-minting-example.hs that includes Main()
4. ExampleMintingPolicy.hs that holds the Haskell


## 8. Define a Plutus minting script that allows you to mint a single instance of a non-fungible token. Your script should take a payment from a user-supplied address and pass this payment to an address of your choice.

## 9. Adapt your solution from Exercise 8 so that you conduct a Dutch auction on your non-fungible token. For example, start the bidding at 1000 Ada and reduce the price by 1 Ada every second. Sell the non-fungible token to the first client that offers to pay at least the current price. When the price falls below your hidden reserve, reject all future bids.

## 10. Adapt your solution from Exercise 9 so that the auction for the non-fungible token starts at a predetermined time. Reject all bids that arrive before the time.

## 11. Optional Exercise (Easy to Moderate)

Publicise your non-fungible token sale and participate in other token sales. Aim to collect the most interesting set of non-fungible tokens. When selling your tokens, you may want to record some metadata on the chain (e.g. representing a digital image or the purchaser's identity) as well as transferring the non-fungible token itself. How can you do this?

## 12. Optional Exercise (Moderate)

Implement a token "factory". You should accept the token, the minting policy and the required number of tokens as parameters to your script and mint the required number of tokens. Does your solution work for both fungible and non-fungible tokens? How do you deal with third-party signatories? Test your solution by allowing another testnet user to mint new tokens using your factory.
